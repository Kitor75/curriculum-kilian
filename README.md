## *Currículum*

![foto](https://gitlab.com/Kitor75/curriculum-kilian/raw/master/Kilian.png)

**Nombre:** Kilian Torres Rubio

**Calle:** Fontanella nº48 1º 1ª, 08291, Barcelona

**Móvil:** 684011290

**Correo:** kitor75@gmail.com

## Actualmente
**Estudiando:** Cursando 1º DAM.

## Estudios
**Grado Medio:** SMIX en ECAIB, Poblenou.

## Idiomas
Idioma | Comprensión oral | Comprensión escrita | Expressión oral | Expresión escrita 
---|---|---|---|---
Catalán | Excelente | Buena | Excelente | Excelente 
Castellano | Excelente | Excelente | Excelente | Excelente
Inglés | Buena | Muy Buena | Normal | Mala 


## Hobbies

>Me gusta descubrir juegos nuevos en el ordenador y nadar.


## Capacidades
* Responsable
* Trabajador
* Autónomo


## Futuro y Aspiraciones
- [x] SMIX
- [ ] Carnet de Coche
- [ ] DAM
- [ ] DAW